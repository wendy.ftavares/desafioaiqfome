# DESAFIO AIQFOME

## Objetivos

- O objetivo desse projeto é garantir que o fluxo de adcionar produto ao carrinho está funcionando corretamente de acordo com o cenário de teste

## Índice

- [Requisitos](#requisitos)
- [Instalação](#instalação)
- [Uso](#uso)
- [Considerações](#considerações)

## Requisitos

- Java JDK 
- IDE de Preferência (Eu utilizei o Eclipse IDE)
- Conta do GitLab

## Instalação

- Após a instalação dos programas listadados acima, faça o download do projeto no Gitlab e importe o projeto maven dentro do Eclipse IDE

Obs1 *IMPORTANTE* : Caso a library do Java seja abaixo da versão SE-1.8 é necessário mudar a library do java manualmente no caminho Propriedades/Java Code Path/Libraries, após acessar o caminho exclua a library obsoleta e adcione uma nova JRE System Library podendo ser a Default ou no meu caso escolhendo a Java SE-1.8 na opção Execution environment.

- Confira se as dependências do Rest Assured e do Junit dentro do arquivo POM

```java
<dependencies>
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-java</artifactId>
			<version>4.12.1</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.13.2</version>
		</dependency>
	</dependencies>
```
Obs2 *IMPORTANTE* : Dentro do projeto existe um chormedriver.exe, então é necessário setar o diretório de acordo com o seu local para que o teste funcione corretamente. No arquivo *src/main/java/WEB/DesafioAiqFome.java* na linha 19 mude o segundo argumento do setProperty, substituindo SEUCAMINHO pelo caminho que o projeto está na sua máquina.

```java
		setProperty("webdriver.chrome.driver",
				"/SEUCAMINHO/ProjetoAiQFome/src/test/resources/chromedriver.exe");
```

- Após salvar já conseguimos fazer os casos de testes dentro do site do Magazine Luiza

## Uso

- Rode os testes como JUnit Test

## Considerações

- O cenário funcionou de acordo com o esperado!

![Evidencia](Evidencia.gif)
