package WEB;

import org.junit.Test;

import static java.lang.System.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;

public class DesafioAiqFome {
	private WebDriver driver;

	@Before
	public void setUp() {
		setProperty("webdriver.chrome.driver",
				"/Users/ender/eclipse-workspace/ProjetoAiQFome/src/test/resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	@Test
	public void AdcionarProduto() throws InterruptedException {

		// Dado que entro no Site do Magazine Luiza
		driver.get("https://www.magazineluiza.com.br/");
		assertEquals("Magazine Luiza | Pra você é Magalu!", driver.getTitle());

		// E entro no departamento de Artesanato
		driver.findElement(By.linkText("Todos os departamentos")).click();
		Thread.sleep(5000);
		driver.findElement(By.linkText("Artesanato")).click();
		Thread.sleep(5000);

		// Quando adicionar o terceiro produto no meu carrinho de compras
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0,200)");
		driver.findElement(By.cssSelector(".sc-dxlmjS:nth-child(3) .sc-wkolL > .sc-dtInlm")).click();
		Thread.sleep(5000);
		js.executeScript("window.scrollTo(0,300)");
		driver.findElement(By.cssSelector("div:nth-child(2) > .kuUZYH > .sc-iHGNWf")).click();

		// Então confiro se os dados do produtos desejado estão corretos
		Thread.sleep(5000);
		js.executeScript("window.scrollTo(0,200)");

		assertEquals("Miçangas Para Pulseira - Letras - C/1000 Peças M26 - Nybc", driver.findElement(By.cssSelector(
				"#root > div > div.App.clearfix > div > div:nth-child(2) > div > div.BasketTable > div.BasketTable-items > div > div.BasketItem-productContainer > div.BasketItemProduct > div > a > p:nth-child(1)"))
				.getText());
		assertEquals("1", driver.findElement(By.cssSelector(
				"#root > div > div.App.clearfix > div > div:nth-child(2) > div > div.BasketTable > div.BasketTable-items > div > div.BasketItem-productContainer > div.BasketItemProduct-quantity > select > option:nth-child(1)"))
				.getText());
		assertTrue(driver.findElement(By.cssSelector("#root > div > div.App.clearfix > div > div:nth-child(2) > div > div.BasketTable > div.BasketTable-items > div > div.BasketItem-productContainer > div.BasketItemProduct-price > span:nth-child(1) > span"))
				.getText().startsWith("R$ 36,96"));
	}

	@After
	public void tearDown() {
		driver.quit();
	}
}
